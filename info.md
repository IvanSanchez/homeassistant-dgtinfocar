
Traffic information and traffic cameras for Spain, from [DGT InfoCar](https://infocar.dgt.es/etraffic/)


See [https://gitlab.com/IvanSanchez/homeassistant-dgtinfocar](https://gitlab.com/IvanSanchez/homeassistant-dgtinfocar) for more information about this integration.
