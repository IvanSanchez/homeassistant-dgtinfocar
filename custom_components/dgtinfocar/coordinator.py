"""DGT InfoCar coordinator: handles updates for traffic sensors"""

import logging

import asyncio
import aiohttp
import json
import sys

from homeassistant.core import callback
# from homeassistant.exceptions import ConfigEntryAuthFailed
from homeassistant.helpers.update_coordinator import (
    CoordinatorEntity,
    DataUpdateCoordinator,
    UpdateFailed,
)

from datetime import timedelta

from .const import DOMAIN

_LOGGER = logging.getLogger(__name__)

session = aiohttp.ClientSession()

class DGTInfoCarCoordinator(DataUpdateCoordinator):
    """DGT InforCar Coordinator."""

    def __init__(self, hass):
        super().__init__(
            hass,
            _LOGGER,
            # Name of the data. For logging purposes.
            name="DGTInfoCarCoordinator",
            # Polling interval. Will only be polled if there are subscribers.
            update_interval=timedelta(seconds=30),
        )

        self._traffic_sensor_ids = set()


    @callback
    def async_add_listener( self, update_callback, context):
        remove_handler = super().async_add_listener(update_callback, context)

        self._traffic_sensor_ids.add(context)

        return remove_handler

    async def _async_update_data(self):
        """Fetch data from DGT InfoCar endpoint."""

        # print ('Should update sensors:', self._traffic_sensor_ids)

        data = {}

        for sensor_id in self._traffic_sensor_ids:
            resp = await session.get(f'https://infocar.dgt.es/etraffic/BuscarElementos?accion=getDetalles&codEle={sensor_id}&tipo=SensorTrafico&indiceMapa=0')
            data[sensor_id] = await resp.json(encoding='UTF-8', content_type=None)

        # print('Update loop done', data)

        return data

