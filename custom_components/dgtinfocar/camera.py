"""DGT InfoCar cameras."""
from __future__ import annotations

from homeassistant.components.camera import (
    Camera
)
from homeassistant.core import HomeAssistant
from homeassistant.helpers.entity_platform import AddEntitiesCallback
from homeassistant.helpers.typing import ConfigType, DiscoveryInfoType
from homeassistant.helpers.entity import EntityCategory

from .const import DOMAIN

import traceback

import datetime

import asyncio
import aiohttp

async def async_setup_entry(hass, config_entry, async_add_entities):
    """Add cameras for passed config_entry."""

    items = config_entry.data['items']

    cameras = []

    for item in items:
        if item['type'] == 'camera':
            cameras.append(DGTInfoCarCamera(
                name=item['name'],
                id=item['id'],
                device_info = {
                    "identifiers": {(DOMAIN, item['name'])},
                    "name": item['name'],
                    "config_entries": [config_entry]
                }
            ))

    async_add_entities(cameras)


session = aiohttp.ClientSession()

class DGTInfoCarCamera(Camera):
    """An individual DGT InfoCar camera entity."""

    def __init__ (self, name=None, id=None, device_info= {}):

        super().__init__()

        # Instance attributes built into Entity:
        self._attr_name = name
        self._available = True
        self._is_streaming = False
        # self._icon = 'mdi:camera'
        self._device_info = device_info
        self._attr_unique_id = f"dgtinfocar_camera_{id}"

        # Instance attributes built into CameraEntity
        self._frame_interval = 300 # 5 minutes
        self._is_on = True
        self._attr_supported_features = 0

        self.camera_id = id
        self._last_image_date = datetime.datetime(year=1970, month=1, day=1)


    async def async_camera_image(
        self, width: int | None = None, height: int | None = None):

        if (datetime.datetime.now() - self._last_image_date).total_seconds() < 120:
            # print(f'Devolviendo imagen antigua de cámara {self.camera_id}')
            return self._image

        # print((datetime.datetime.now() - self._last_image_date).total_seconds())
        # print(f'Pidiendo imagen de cámara {self.camera_id}')

        resp = await session.get(f'https://infocar.dgt.es/etraffic/data/camaras/{self.camera_id}.jpg')
        self._image = await resp.read()
        self._last_image_date = datetime.datetime.now()
        return self._image


    async def async_refresh_providers(self):
        # Somehow needed in order to not crash. By default, HASS expects RTSP
        # streams to be defined despite camera capabilities not including streaming
        pass
