"""Config flow for DGT InfoCar"""
from __future__ import annotations

from homeassistant.data_entry_flow import FlowResult
from homeassistant import config_entries
import homeassistant.helpers.config_validation as cv
import voluptuous as vol
import logging
from .const import DOMAIN
from homeassistant.const import (
    CONF_LATITUDE,
    CONF_LOCATION,
    CONF_LONGITUDE,
    CONF_RADIUS,
    UnitOfLength,
)
from homeassistant.helpers.selector import (
    LocationSelector,
    NumberSelector,
    NumberSelectorConfig,
)

from math import (pi,cos)
import aiohttp

DEFAULT_RADIUS = 0.5

# Earth’s radius, sphere
R=6378137

_LOGGER = logging.getLogger(__name__)

class DGTInfoCarConfigFlow(config_entries.ConfigFlow, domain=DOMAIN):

    VERSION = 1

    CONNECTION_CLASS = config_entries.CONN_CLASS_LOCAL_PUSH

    session = aiohttp.ClientSession()

    async def async_step_user( self, user_input ) -> FlowResult:
        """Handle the user-driven step: ask the user for a point in a map."""

        if not user_input:
            return self._show_form_user()

        lat = user_input['location']['latitude']
        lng = user_input['location']['longitude']
        radiusKm = user_input['radius']
        radius = radiusKm * 1000
        deltaLat = (180/pi) * radius/R
        deltaLon = (180/pi) * radius/(R*cos(pi*lat/180))

        self._config_name = f'{lng:3.2f},{lat:3.2f} {radiusKm:2.1f}Km'

        url = f'https://infocar.dgt.es/etraffic/BuscarElementos?latNS={lat+deltaLat}&longNS={lng + deltaLon}&latSW={lat - deltaLat}&longSW={lng - deltaLon}&zoom=17&accion=getElementos&Camaras=true&SensoresTrafico=true&SensoresMeteorologico=false&Paneles=false&Radares=false&IncidenciasRETENCION=false&IncidenciasOBRAS=false&IncidenciasMETEOROLOGICA=false&IncidenciasPUERTOS=false&IncidenciasOTROS=false&IncidenciasEVENTOS=false&IncidenciasRESTRICCIONES=false&niveles=false&caracter=acontecimiento'

        resp = await self.session.get(url)
        points = await resp.json(encoding='UTF-8', content_type=None)

        print(points)

        def f(p):
            if p['tipo'] == 'Camara':
                return {'type': 'camera', 'name': p['alias'], 'id':p['codEle']}
            if p['tipo'] == 'SensorTrafico':
                return {'type': 'traffic_sensor', 'name': p['alias'], 'id':p['codEle']
                        # , 'direction': p['sentido']
                        }
            else:
                return {}

        self._items = [f(p) for p in points]


        return await self.async_step_select_items()

    async def async_step_select_items( self, user_input = None) -> FlowResult:

        if user_input is not None:
            r = None
            def clear_name(x):
                y = x.copy()
                del y['name']
                return y

            items = []

            for name in user_input['items']:
                # Search for items with the given name
                matching_items = [i for i in self._items if i['name']==name ]
                items.extend(matching_items)

            if (len(items)== 0):
                return self.async_abort(reason="no_items_selected");

            # matching_items = [clear_name(i) for i in matching_items]
            # print('creating entry for', name, items)
            return self.async_create_entry(
                title = self._config_name,
                data = {'items': items}
            )

        def n(p):
            return p['name']

        return self.async_show_form(
            step_id="select_items",
            description_placeholders={"items_count": str(len(self._items))},
            data_schema=vol.Schema(
                {vol.Required('items'): cv.multi_select([n(p) for p in self._items])}
            ),
        )





    def _show_form_user(
        self,
        user_input: dict[str, Any] | None = None,
        errors: dict[str, Any] | None = None,
    ) -> FlowResult:
        if user_input is None:
            user_input = {}
        return self.async_show_form(
            step_id="user",
            data_schema=vol.Schema(
                {
                    # vol.Required(
                    #     CONF_NAME, default=user_input.get(CONF_NAME, "")
                    # ): cv.string,
                    # vol.Required(
                    #     CONF_API_KEY, default=user_input.get(CONF_API_KEY, "")
                    # ): cv.string,
                    # vol.Required(
                    #     CONF_FUEL_TYPES,
                    #     default=user_input.get(CONF_FUEL_TYPES, list(FUEL_TYPES)),
                    # ): cv.multi_select(FUEL_TYPES),
                    vol.Required(
                        CONF_LOCATION,
                        default=user_input.get(
                            CONF_LOCATION,
                            {
                                "latitude": self.hass.config.latitude,
                                "longitude": self.hass.config.longitude,
                            },
                        ),
                    ): LocationSelector(),
                    vol.Required(
                        CONF_RADIUS, default=user_input.get(CONF_RADIUS, DEFAULT_RADIUS)
                    ): NumberSelector(
                        NumberSelectorConfig(
                            min=1.0,
                            max=5.0,
                            step=0.1,
                            unit_of_measurement=UnitOfLength.KILOMETERS,
                        ),
                    ),
                }
            ),
            errors=errors,
        )


