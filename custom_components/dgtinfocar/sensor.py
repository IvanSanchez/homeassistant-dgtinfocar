"""DGT InfoCar cameras."""
from __future__ import annotations

from homeassistant.components.sensor import (
    SensorDeviceClass,
    SensorEntity,
    SensorStateClass,
)
from homeassistant.core import (
    HomeAssistant,
    callback
)
from homeassistant.helpers.entity_platform import AddEntitiesCallback
from homeassistant.helpers.typing import ConfigType, DiscoveryInfoType
from homeassistant.helpers.entity import EntityCategory

from homeassistant.const import (
    UnitOfSpeed,
    PERCENTAGE
)
from homeassistant.helpers.update_coordinator import (
    CoordinatorEntity,
    DataUpdateCoordinator,
    UpdateFailed,
)
from .const import DOMAIN
from .coordinator import DGTInfoCarCoordinator

import traceback

import datetime

import asyncio
import aiohttp

async def async_setup_entry(hass, config_entry, async_add_entities):
    """Add cameras for passed config_entry."""

    coordinator = DGTInfoCarCoordinator(hass)

    items = config_entry.data['items']

    sensors = []

    # 4 sensores por item de sensor de tráfico:
    # - Intensidad (vehículos/hora)
    # - Velocidad media (km/h)
    # - % ocupación
    # - % ligeros

    for item in items:
        if item['type'] == 'traffic_sensor':

            common_data = {
                'state_class': SensorStateClass.MEASUREMENT,
                'coordinator': coordinator,
                'device_info': {
                    "identifiers": {(DOMAIN, item['name'])},
                    "name": item['name'],
                    "config_entries": [config_entry]
                }
            }

            sensors.extend([
                DGTInfoCarSensor(
                    name=f'Traffic Intensity',
                    id=item['id'],
                    native_unit_of_measurement='vehicles/h',
                    json_field='intensidad',
                    icon='mdi:car-clock',
                    **common_data
                ),
                DGTInfoCarSensor(
                    name=f'Speed',
                    id=item['id'],
                    device_class=SensorDeviceClass.SPEED,
                    native_unit_of_measurement=UnitOfSpeed.KILOMETERS_PER_HOUR,
                    icon='mdi:speedometer',
                    json_field='velocidad',
                    **common_data
                ),
                DGTInfoCarSensor(
                    name=f'Occupation',
                    id=item['id'],
                    native_unit_of_measurement=PERCENTAGE,
                    json_field='ocupacion',
                    icon='mdi:car-multiple',
                    **common_data
                ),
                DGTInfoCarSensor(
                    name=f'Light vehicles',
                    id=item['id'],
                    native_unit_of_measurement=PERCENTAGE,
                    json_field='composicion',
                    icon='mdi:car-hatchback',
                    **common_data
                ),
            ])

    async_add_entities(sensors)


session = aiohttp.ClientSession()

class DGTInfoCarSensor(CoordinatorEntity, SensorEntity):
    """An individual DGT InfoCar camera entity."""

    def __init__ (self,
                 name = None,
                 icon = None,
                 entity_category = None,
                 device_class = None,
                 device_info = None,

                 state_class= None,
                 native_unit_of_measurement=None,

                 id = None,
                 coordinator = None,
                 json_field = None,
                 ):

        """Pass coordinator to CoordinatorEntity."""
        super().__init__(coordinator, context=id)

        # Instance attributes built into Entity:
        self._attr_name = f"{device_info['name']} {name}"
        self._attr_available = True
        self._attr_icon = icon
        self._attr_device_info = device_info
        self._attr_unique_id = f"dgtinfocar_sensor_{id}_{json_field}"

        # Instance attributes built into SensorEntity:
        self._attr_state_class = state_class
        self._attr_native_value = None
        self._attr_native_unit_of_measurement = native_unit_of_measurement

        self.sensor_id = id
        self.json_field = json_field


    @callback
    def _handle_coordinator_update(self) -> None:
        """Handle updated data from the coordinator."""

        # print('Handling data:', self.coordinator.data[self.sensor_id])

        sensor_data = self.coordinator.data[self.sensor_id]

        if sensor_data is not None:
            value = sensor_data.get(self.json_field)

            if value is None or value == '--' or sensor_data.get('noDatos'):
                self._attr_available = False
                self.async_write_ha_state()
            else:
                self._attr_available = True
                self._attr_native_value = value
                self.async_write_ha_state()


