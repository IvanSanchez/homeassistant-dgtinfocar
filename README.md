# homeassistant-dgtinfocar

`homeassistant-dgtinfocar` is a [Home Assistant](https://www.home-assistant.io) integration for [DGT InfoCar](https://infocar.dgt.es/etraffic/) traffic cameras and traffic density sensors.

## Installation

#### Guided installation

- Enable the [HACS](https://hacs.xyz/) integration in your Home Assistant instance.
- Use the side menu to browse HACS.
- Navigate to "Integrations", then use the overflow menu (three dots at the top-left) to add a Custom Repository.
- Enter the URL `https://github.com/IvanSanchez/homeassistant-dgtinfocar`, of type "Integration"
- You should see a new box labelled "DGT InfoCar". Click on it and follow HACS' instructions to download and enable the integration.
- Restart Home Assistant when HACS tells you to.

#### Manual installation

Download the files from this repository. Copy the `custom_components/dgtinfocar/` directory into the `custom_components` directory of your Home Assistant instance.

e.g. if your configuration file is in `/home/homeassistant/.homeassistant/configuration.yaml`, then the files from this integration should be copied to `/home/homeassistant/.homeassistant/custom_components/dgtinfocar/`.

Restart Home Assistant to ensure the integration can be detected.

## Usage

Use the Home Assistant GUI to add a new integration (settings → devices & services → add new integration). You should find the DGT InfoCar integration in the list.

Select a location (by dragging the marker on the map) and a search radius. The next step will show a list of cameras & sensors within that search radius; select all of interest to you.

![Screenshot of DGT InfoCar sensors in Home Assistant](./screenshot.png)

## Bugs? Comments?

Use the gitlab issue tracker at https://gitlab.com/IvanSanchez/homeassistant-dgtinfocar/-/issues

(Yes, it's Git**Lab** and not Git**Hub**. Development happens at GitLab. The GitHub repo is only for HACS compatibility.)

Please keep in mind that it's an issue tracker, and not a discussion forum. I recommend reading ["How to Report Bugs Effectively"](https://www.chiark.greenend.org.uk/~sgtatham/bugs.html) if you've never written into an issue tracker before.

<!--Please provide any Home Assistant logs. It's a good idea to increase the verbosity of the logs for the `freeds` integration by adding this to yout `configuration.yml` file (as explained at https://www.home-assistant.io/integrations/logger/ ):

```yaml
logger:
  default: warning
  logs:
    custom_components.dgtinfocar: info
```-->

## License

Licensed under GPLv3. See the `LICENSE` file for details.
